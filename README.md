# SNP Population Frequency Service (for use with docker or docker-compose)

This microservice returns population frequency data for certain SNPs. It is based on the May 2021 release of the dbSNP population frequency files which can be found [here](https://ftp.ncbi.nih.gov/snp/population_frequency/latest_release/). Parsing is done using Java (Quarkus project) and [HTSlib](https://github.com/samtools/htslib)

## Setup

- Install Maven
- Clone the git project (Repo to be found [here](https://gitlab.gwdg.de/UKEB/mtb/freqservice/-/tree/dev/docker))
- Navigate to the main folder and call `./mvnw clean package`. If permissions are necessary, call `chmod +x ./mvnw` beforehand. This should create a docker image containing the project. Note that this image contains a line setting user and group ID, this is done specifically for the MTB vm and *may* cause permission issues unless removed/changed.
- Now there are two ways to start up the project:
  * **Use Docker-compose:** Configure the docker-compose.yml. The bind mount volume must point to a single folder containing the *freq.vcf.gz* and *chromosome_accessors* files. If the port is changed, make sure to also change it inside Quarkus's *application.properties* and the according *Dockerfile.jvm*. The image also has to be tagged `frequencyservice:latest` using `docker tag`. Call `docker-compose up (-d)` to start the container.
  * **Use a standalone container:** Simply start the container, e.g. `docker run --rm -p 8090:8090 -v /path/to/files:/mnt imagename`.
- The application is ready for querying now. If no tbi file is present, it might take a while to build one, but this only has to be done once per machine.

# Querying the service

For single queries, it is best to use the Swagger UI at `dbsnp/v1/doc`.

The service accepts variants in the format `chrN:positionREF>VAR`. Th position can be a range (`M-N`).
The genome must also be stated - default value is hg38, but hg37 will also be valid in the future.  

Genomic variants are added to the endpoints as query parameters (comma separated), called **genompos**.
N can be any number between 1 and 24, X or Y (23/24 are equivalent to X and Y).
The return format is always JSON.
If cURL is used, the `>` has to be replaced with the HTTP accessor `%3E` to avoid encoding problems.

## Processing endpoints

Currently, there are three processing endpoints:    

  * `dbsnp/v1/{genome}/full`: Delivers full JSON output of all population frequencies for that variant, as well as additional genomic and metadata.
   
  * `dbsnp/v1/{genome}/simple`: Omits percentages from the output.
  
  * `dbsnp/v1/{genome}/total`: Only returns the counts and percentage of the total population frequency.

## Metadata endpoint

Metadata for the tool can be queried using the following endpoint:
  
  * `dbsnp/v1/info`: Shows some metadata regarding the tool.  

---

## Usage Example

Example call, assuming that the service runs on port 8090: (Note the replacement of the `>` as described above)

`curl -X GET "http://localhost:8090/dbsnp/v1/hg38/full?genompos=chr1:12975309TTCTC%3ET,chr17:g.7673776G%3EA" | jq`  

---

An older version of this project using a PostGreSQLdb can be found [here](https://gitlab.gwdg.de/UKEB/mtb/onkopus/snpservices/frequencyservice).


## Database Files

The population frequency database has been downloaded from:

https://ftp.ncbi.nih.gov/snp/population_frequency/latest_release/



