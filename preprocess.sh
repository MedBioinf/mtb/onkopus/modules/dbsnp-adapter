#!/bin/bash

FREQ_URL="https://ftp-trace.ncbi.nih.gov/snp/population_frequency/latest_release/"

# Check if database files are present, download them if they cannot be found
if [ ! -f "${DATA_PATH}/freq.vcf.gz" ]
then
  echo "Could not find database files. Starting file download..."
  cd "${DATA_PATH}"

  wget -v "${FREQ_URL}freq.vcf.gz" -P "${DATA_PATH}"
  wget -v "${FREQ_URL}freq.vcf.gz.tbi" -P "${DATA_PATH}"
else
  echo "Database files found in ${OUT_DIR}"
fi

/opt/venv/bin/python3 /app/app.py



