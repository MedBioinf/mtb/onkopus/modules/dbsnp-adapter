import traceback, re, subprocess
import conf.read_config as conf_reader
import logging


def generate_tabix_query(variant_list):
    """


    :param variant_list:
    :return:
    """
    response = {}
    genome_pos_exp = "(chr)([0-9|X|Y]+):(g\.)?([0-9]+)([A|G|C|T|N]+)>([A|G|C|T|N]+)"
    p = re.compile(genome_pos_exp)

    tabix_q = ''
    for variant in variant_list:
        response[variant] = {}

        try:
            groups = p.match(variant)
            chrom = groups.group(2)
            pos = groups.group(4)
            #ref = groups.group(4)
            #alt = groups.group(5)
            #print(groups.groups())

            #tabix_q += chrom + ":" + pos + "-" + pos + " "
            tabix_q += get_tabix_positions("chr"+chrom, pos)
        except:
            logger = logging.getLogger('root')
            logger.debug("Could not parse genomic location: ",variant,traceback.format_exc())

    return tabix_q


def get_dbsnp_entry(variant_list, genome_version):
    """

    :param variant_list:
    :param genome_version:
    :return:
    """

    db_file = conf_reader.__DATA_PATH__ + "/" + conf_reader.__FREQUENCY_FILE__

    tabix_q = generate_tabix_query(variant_list)
    tabix_q = tabix_q.rstrip(" ")

    tabix_query = "tabix " + db_file + " "+ tabix_q
    logger = logging.getLogger('root')
    logger.debug(tabix_query)

    process = subprocess.Popen(tabix_query.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()
    output = output.decode("utf-8")

    results = output.split('\n')
    response = {}
    for result in results:
        elements = result.split("\t")
        if len(elements) > 1:
            chrom_res = elements[0]
            chrom_res = _get_chromosome_identifier(chrom_res)

            pos_res = elements[1]
            ref_res = elements[3]
            alt_res = elements[4]

            alt_alleles = alt_res.split(",")
            for i,alt_allele in enumerate(alt_alleles):
                var_allele_index=i
                rs_id = elements[2]
                var = chrom_res + ":" + pos_res + ref_res + ">" + alt_allele
                var_gen = chrom_res + ":g." + pos_res + ref_res + ">" + alt_allele
                if var in variant_list:
                    if var not in response:
                        response[var] = {}
                    response[var][conf_reader.config["DEFAULT"]["MODULE_PREFIX"]] = generate_json_obj_from_database_array(elements,var_allele_index)
                elif var_gen in variant_list:
                    if var_gen not in response:
                        response[var_gen] = {}
                    response[var_gen][conf_reader.config["DEFAULT"]["MODULE_PREFIX"]] = generate_json_obj_from_database_array(
                        elements, var_allele_index)
                else:
                    logger = logging.getLogger('root')
                    msg = "variant not found: ",str(var),": ",str(variant_list).format()
                    logger.info(msg)
    return response


def generate_json_obj_from_database_array(elements,var_allele_index):
    """
    Generates a JSON dictionary from a dbNSFP database entry array

    :param elements:
    :return:
    """
    json_obj = {}

    columns = ["chromosome", "start", "rsID", "refAllele", "varAllele", "quality", "filter", "info", "format", "freq_european", "freq_africanOthers", "freq_eastAsian",
    "freq_americanAmerican", "freq_latinAmerican1", "freq_latinAmerican2", "freq_otherAsian", "freq_southAsian",
    "freq_other", "freq_african", "freq_asian", "freq_total", "q_id"]


    info_col = elements[7]
    features = elements[7].split(";")
    for feature in features:
        if len(feature.split("="))>1:
            key,val = feature.split("=")
            json_obj[key] = val

    for i in range(9,21):
        values = elements[i].split(":")

        ref_count, var_count = values[0], values[1].split(",")[var_allele_index]
        percentage = 0
        try:
            if float(ref_count)!=0:
                percentage = float( var_count) / float(ref_count)
            else:
                percentage = float(0.0)
        except:
            logger = logging.getLogger('root')
            msg = "Could not calculate percentage "+traceback.format_exc().format()
            logger.info(msg)
        json_obj[columns[i]] = var_count+":"+ref_count + "(" + str(percentage) + ")"

    json_obj["rsID"] = elements[2]

    return json_obj


def get_tabix_positions(chrom, pos):
    """
    Retrieves the NCBI chromosome identifiers and adds a tabix query for each chromosome identifier

    :param chrom:
    :param pos:
    :return:
    """
    q=""
    chrom_list = _get_chromosome_accessor(chrom)
    for ncbi_chrom in chrom_list:
        q+= ncbi_chrom+ ":" + pos + "-" + pos + " "
    #q = q.rstrip(" ")
    return q


def _get_chromosome_identifier(chrom):
    """
    Maps NCBI chromosome identifiers to chromosome identifiers

    :param chrom: Chromosome identifier, e.g. "NC_000001.9"
    :return:
    """
    accessors = {
        "NC_000001.9": "chr1",
        "NC_000001.10": "chr1",
        "NC_000001.11": "chr1",
        "NC_000002.11": "chr2",
        "NC_000002.12": "chr2",
        "NC_000003.11": "chr3",
        "NC_000003.12": "chr3",
        "NC_000004.11": "chr4",
        "NC_000004.12": "chr4",
        "NC_000005.9": "chr5",
        "NC_000005.10": "chr5",
        "NC_000006.11": "chr6",
        "NC_000006.12": "chr6",
        "NC_000007.13": "chr7",
        "NC_000007.14": "chr7",
        "NC_000008.10": "chr8",
        "NC_000008.11": "chr8",
        "NC_000009.11": "chr9",
        "NC_000009.12": "chr9",
        "NC_000010.10": "chr10",
        "NC_000010.11": "chr10",
        "NC_000011.9": "chr11",
        "NC_000011.10": "chr11",
        "NC_000012.11": "chr12",
        "NC_000012.12": "chr12",
        "NC_000013.10": "chr13",
        "NC_000013.11": "chr13",
        "NC_000014.8": "chr14",
        "NC_000014.9": "chr14",
        "NC_000015.9": "chr15",
        "NC_000015.10": "chr15",
        "NC_000016.8": "chr16",
        "NC_000016.9": "chr16",
        "NC_000016.10": "chr16",
        "NC_000017.10": "chr17",
        "NC_000017.11": "chr17",
        "NC_000018.9": "chr18",
        "NC_000018.10": "chr18",
        "NC_000019.9": "chr19",
        "NC_000019.10": "chr19",
        "NC_000020.10": "chr20",
        "NC_000020.11": "chr20",
        "NC_000021.8": "chr21",
        "NC_000021.9": "chr21",
        "NC_000022.10": "chr22",
        "NC_000022.11": "chr22",
        "NC_000023.9": "chr23",
        "NC_000023.10": "chr23",
        "NC_000023.11": "chr23",
        "NC_000024.10": "chr24",
        "NC_012920.1": "mitoc",
    }
    if chrom in accessors:
        return accessors[chrom]
    else:
        logger = logging.getLogger('root')
        logger.debug("Could not find accessor: ",chrom)
        return []


def _get_chromosome_accessor(chrom):
    """
    Maps chromosome identifiers to NCBI chromosome identifiers

    :param chrom: Chromosome identifier, e.g. "chr1"
    :return:
    """
    accessors = {
        "chr1": ["NC_000001.9","NC_000001.10","NC_000001.11"],
        "chr2": ["NC_000002.11","NC_000002.12"],
        "chr3": ["NC_000003.11","NC_000003.12"],
        "chr4": ["NC_000004.11","NC_000004.12"],
        "chr5": ["NC_000005.9","NC_000005.10"],
        "chr6": ["NC_000006.11","NC_000006.12"],
        "chr7": ["NC_000007.13","NC_000007.14"],
        "chr8": ["NC_000008.10","NC_000008.11"],
        "chr9": ["NC_000009.11","NC_000009.12"],
        "chr10": ["NC_000010.10","NC_000010.11"],
        "chr11": ["NC_000011.9","NC_000011.10"],
        "chr12": ["NC_000012.11","NC_000012.12"],
        "chr13": ["NC_000013.10","NC_000013.11"],
        "chr14": ["NC_000014.8","NC_000014.9"],
        "chr15": ["NC_000015.9","NC_000015.10"],
        "chr16": ["NC_000016.8","NC_000016.9","NC_000016.10"],
        "chr17": ["NC_000017.10","NC_000017.11"],
        "chr18": ["NC_000018.9","NC_000018.10"],
        "chr19": ["NC_000019.9","NC_000019.10"],
        "chr20": ["NC_000020.10","NC_000020.11"],
        "chr21": ["NC_000021.8","NC_000021.9"],
        "chr22": ["NC_000022.10","NC_000022.11"],
        "chr23": ["NC_000023.9","NC_000023.10","NC_000023.11"],
        "chr24": ["NC_000024.10"],
        "mitoc": ["NC_012920.1"],
        "chrX": ["NC_000023.9", "NC_000023.10", "NC_000023.11"],
        "chrY": ["NC_000024.10"]
    }
    if chrom in accessors:
        return accessors[chrom]
    else:
        logger = logging.getLogger('root')
        logger.debug("Could not find accessor: ",chrom)
        return []
