import unittest
import dbsnp_adapter.data_parser

class testResultParsing(unittest.TestCase):

    def test_dbsnp(self):
        elements = [
            "NC_000007.12","1820789","rs2115066191","A","G",".",".",".","AN:AC:HWEP:GR:GV:GA",
            "38172:8058:1:11906:6302:878",
            "36:34:0:0:2:16",
            "56:16:1:13:14:1",
            "1192:920:0:30:212:354",
            "18:5:0:5:3:1",
            "70:27:0:13:17:5",
            "10:3:1:2:3:0",
            "4836:1997:1:847:1145:426",
            "450:159:3:101:89:35",
            "1228:954:0:30:214:370",
            "66:19:1:15:17:1",
            "44840:11219:32:12917:7787:1716"
        ]
        var_allele_index=0
        response = dbsnp_adapter.data_parser.generate_json_obj_from_database_array(elements, var_allele_index)

        print(response)

