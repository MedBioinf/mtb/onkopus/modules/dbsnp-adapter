import unittest, time
from app import app

class TestGetIDs(unittest.TestCase):

    def setUp(self):
        self.ctx = app.app_context()
        self.ctx.push()
        self.client = app.test_client()

    def tearDown(self):
        self.ctx.pop()

    def test_dbsnp_service(self):
        start_time = time.time()
        response = self.client.get('/dbsnp/v1/hg38/full?genompos=chr1:12975309TTCTC>T,chr17:g.7673776G>A')
        print(response.get_data(as_text=True))
        end_time = time.time() - start_time
        print("Time: ",end_time)

