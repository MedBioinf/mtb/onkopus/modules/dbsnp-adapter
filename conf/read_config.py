import os, configparser

config = configparser.ConfigParser()
config.read(os.path.join(os.path.dirname(__file__), '', 'config.ini'))

if "MODULE_SERVER" in os.environ:
    __MODULE_SERVER__ = os.getenv("MODULE_SERVER")
else:
    __MODULE_SERVER__ = config['DEFAULT']['MODULE_SERVER']

if "DATA_PATH" in os.environ:
    __DATA_PATH__ = os.getenv("DATA_PATH")
else:
    __DATA_PATH__ = config['DBSNP']['DATA_PATH']

if "PORT" in os.environ:
    __PORT__ = os.getenv("PORT")
else:
    __PORT__ = config['DBSNP']['DBSNP_ADAPTER_PORT']

if "FREQUENCY_FILE" in os.environ:
    __FREQUENCY_FILE__ = os.getenv("FREQUENCY_FILE")
else:
    __FREQUENCY_FILE__ = config['DBSNP']['FREQUENCY_FILE']

if "LOG_PATH" in os.environ:
    __LOG_PATH__ = os.getenv("LOG_PATH")
else:
    __LOG_PATH__ = config['DEFAULT']['LOG_PATH']

if "LOG_FILE" in os.environ:
    __LOG_FILE__ = os.getenv("LOG_FILE")
else:
    __LOG_FILE__ = config['DEFAULT']['LOG_FILE']
